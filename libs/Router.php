<?php 
    class Router {

        private static $url = [];//armazena o URL

        private static $argument = [];//armazena os argumentos da URL

        public function __construct()
        {
            $this->parseUrl();
            $this->setArgument();
            $this->initController();
           
        }

        //verifica a URL (localhost/mvcex/index/view/arguments) e tranforma em um array ( [localhost,mvcex,index,view,arguments] )
        private function parseUrl()
        {
            self::$url = isset ($_GET["url"]) ? $_GET["url"] : "index";

            self::$url = rtrim(self:: $url, "/"); // retira espaços em branco

            self::$url = explode("/", self::$url); // separa cada palavra 
        }

        //armazena a URL =  [localhost,mvc,index,view,arguments]
        private static function setArgument()
        {
            if(isset(self::$url[2]))
            {
                $temp = self::$url;

                unset($temp[0], $temp[1]); //[NULL ,NULL ,index,view,arguments]

                self::$argument = array_merge($temp); //[index,view, arguments]
            }
        }

       //coloca a primeira letra  de $url[0] como maiúscula e concatena coma a palavra Controller
       // $url[0] = index => Index+Controler => IndexControler
        private function getController() 
        {
            return ucfirst(self::$url[0]) . "Controller"; 
        }
 
        //coloca a primeira letra  de $url[0] como maiúscula e concatena coma a palavra Model
        //$url[0] = index => Index+Model => IndexModel
        private function getModel() 
        {
            return ucfirst(self::$url[0]) . "Model";
        }

        private function initController()
        {
            $controllerClass = $this->getController();//recebe o nome do controller
            $modelClass = $this->getModel(); //recebe o nome da model
            $this->loadController($controllerClass, $modelClass);//(IndexController, IndexModel)
        }

        //Procura pelos arquivos, se achar cria uma instância
        private function loadController($controllerClass, $modelClass)         {
                                    // (IndexController, IndexModel)

            if (!file_exists(BASE_PATH . DS . "controllers" . DS . $controllerClass . ".php")) 
            {
                $this->error404();
            } 
            else 
            {
                $controller = new $controllerClass(new $modelClass());

               //url=[index,view, arguments]
                if(isset(self::$url[2]))
                {
                    $this->runActionArguments($controller);
                }
                else if(isset(self::$url[1]))
                {
                    $this->runAction($controller);
                }
                else
                {
                    $this->runDefaultAction($controller);
                }
            }
        }

        private function error404() 
        {
            //fazer esse método depois
        }

        private function runActionArguments(Controller $controller)
        {
            // //url=[index,view, arguments]
            if(method_exists($controller, self::$url[1]))
            {               
                call_user_func_array([$controller, self::$url[1]], self::$argument);
            } else
            {
                $this->error404();
            }
        }       

        private function runAction(Controller $controller)
        {
            //url=[index,view, arguments]
            if(method_exists($controller, self::$url[1]))
            {               
                $controller->{self::$url[1]}();// controller->view();
            } else
            {
                $this->error404();
            }
        } 

        private function runDefaultAction(Controller $controller) 
        {
            if (method_exists($controller, "index")) 
            {
                $controller->index();
            }
        }

    }
?>