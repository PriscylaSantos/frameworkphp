<?php
class Loader {

    public function __construct() {
 
    }
     
    public function loadClass()
    {
        function __autoload($classname) 
        {
            $controllerClass = BASE_PATH . DS . "controllers" . DS . $classname . ".php";

            $modelClass = BASE_PATH . DS . "models" . DS . $classname . ".php";
            
            $lib = BASE_PATH . DS . "lib" . DS . $classname . ".php";
            
            if (file_exists($controllerClass)) {
                require_once($controllerClass);
                 
            } else if (file_exists($modelClass)) {
                require_once($modelClass);
                 
            } else if (file_exists($lib)) {
                require_once($lib);
                 
            } else {
                throw new Exception("Error loading files");
            }
        }
         
    }
}
?>