<?php
    class IndexController extends Controller
    {
        public function __construct(Model $model)
        {
            parent::__construct($model);
        }

        public function index()
        {
            $data["title"] = "PHP MVC Framework";
            $this->view->render("index/index", $data);
        }        
    }
?>