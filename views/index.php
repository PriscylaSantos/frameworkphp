<?php
    //localhost/mvc/
    $url = $_SERVER["HTTP_HOST"] ."/mvc/"; 
    
    define("URL", $url);
    define("DS", DIRECTORY_SEPARATOR); // representa "/"
    define("BASE_PATH", $_SERVER['DOCUMENT_ROOT']);

    require_once(BASE_PATH.DS."libs".DS."Loader.php");
    require_once(BASE_PATH.DS."libs".DS."Router.php");

    $load = new Loader();
    $load->loadClass();
    $router = new Router();
?>